#ifndef ANDROID_SRS_TECH_HEADERS
#define ANDROID_SRS_TECH_HEADERS

#include <stdint.h>
#include <sys/types.h>
#include <limits.h>
#include <utils/Log.h>
#include <utils/String8.h>
#include <math.h>
#include <cutils/log.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "srs_setup.h"
#include "srs_processing.h"
#include "srs_params.h"
#include "srs_workspace.h"
#include "srs_tech.h"
#include "srs_techs/srs_tech_tools.h"
#include "srs_subs/srs_routing.h"

#endif	// ANDROID_SRS_TECH_HEADERS

